<?php
/**
 * Created by PhpStorm.
 * User: Alegria
 * Date: 08/03/2018
 * Time: 07:06 PM
 */

class Punto {

    function __construct($p) {
         $this->id = $p->id;
         $this->necesita_lider = $p->necesita_lider;
         $this->principal = $p->principal;
         $this->transversal = $p->transversal;
         $this->referencia = $p->referencia;
         $this->observaciones = $p->observaciones;
         $this->latitud = $p->latitud;
         $this->longitud = $p->longitud;
         $this->zonas_id = $p->zonas_id;
         $this->turno1_Colecta = $p->turno1_Colecta;
         $this->turno1_observacion = $p->turno1_observacion;
         $this->turno2_Colecta = $p->turno2_Colecta;
         $this->turno2_observacion = $p->turno2_observacion;
         $this->turno3_Colecta = $p->turno3_Colecta;
         $this->turno3_observacion = $p->turno3_observacion;
         $this->turno4_Colecta = $p->turno4_Colecta;
         $this->turno4_observacion = $p->turno4_observacion;
         $this->turno5_Colecta = $p->turno5_Colecta;
         $this->turno5_observacion = $p->turno5_observacion;
         $this->turno6_Colecta = $p->turno6_Colecta;
         $this->turno6_observacion = $p->turno6_observacion;
         $this->turno7_Colecta = $p->turno7_Colecta;
         $this->turno7_observacion = $p->turno7_observacion;
         $this->turno8_Colecta = $p->turno8_Colecta;
         $this->turno8_observacion = $p->turno8_observacion;
         $this->turno9_Colecta = $p->turno9_Colecta;
         $this->turno9_Calle = $p->turno9_Calle;
         $this->turno9_observacion = $p->turno9_observacion;
    }

    public $id; //String
    public $necesita_lider; //String
    public $principal; //String
    public $transversal; //String
    public $referencia; //String
    public $observaciones; //String
    public $latitud; //String
    public $longitud; //String
    public $zonas_id; //String
    public $turno1_Colecta; //String
    public $turno1_observacion; //String
    public $turno2_Colecta; //String
    public $turno2_observacion; //String
    public $turno3_Colecta; //String
    public $turno3_observacion; //String
    public $turno4_Colecta; //String
    public $turno4_observacion; //String
    public $turno5_Colecta; //String
    public $turno5_observacion; //String
    public $turno6_Colecta; //String
    public $turno6_observacion; //String
    public $turno7_Colecta; //String
    public $turno7_observacion; //String
    public $turno8_Colecta; //String
    public $turno8_observacion; //String
    public $turno9_Colecta; //String
    public $turno9_Calle; //String
    public $turno9_observacion; //String
}
