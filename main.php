<?php
/**
 * Created by PhpStorm.
 * User: Alegria
 * Date: 08/03/2018
 * Time: 08:19 PM
 */

require_once __DIR__ . '/data/Repository.php';
require_once __DIR__ . '/models/Punto.php';

foreach (Repository::getPuntos() as $p) {

    $punto = new Punto($p);

    echo $punto->id . "<br>";
}