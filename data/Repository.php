<?php
/**
 * Created by PhpStorm.
 * User: Alegria
 * Date: 08/03/2018
 * Time: 07:41 PM
 */

require_once __DIR__ . "/../libs/JsonMapper.php";
require_once __DIR__ . "/../models/Punto.php";

class Repository
{

    const BASE_URL = "http://nicaragua.colecta.techo.org/api/";
    const ENDPOINT_PUNTOS = "puntos";

    private static $mapper;

    /**
     * Returns a list of Puntos
     * @return ArrayObject
     */
    public static function getPuntos() {

        $json = self::getJsonResponseFrom(self::ENDPOINT_PUNTOS);
        $mapper = self::getMapperInstance();

        $puntos = $mapper->mapArray($json, array(), 'Punto');

        return $puntos;
    }

    private static function getJsonResponseFrom($endpoint) {
        $response = file_get_contents(self::getEnpoint($endpoint));
        $json = json_decode($response);
        return $json;
    }

    private static function getEnpoint($enpoint) {
        return self::BASE_URL . $enpoint;
    }

    private static function getMapperInstance() {
        if(!isset(self::$mapper)) {
            self::$mapper = new JsonMapper();
            self::$mapper->bStrictNullTypes = false;
        }
        return self::$mapper;
    }

}